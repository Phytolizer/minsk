using System;
using Minsk.CodeAnalysis.Binding;
using Minsk.CodeAnalysis.Syntax;

namespace Minsk.CodeAnalysis
{
    internal sealed class Evaluator
    {
        private readonly BoundExpression _root;

        public Evaluator(BoundExpression root)
        {
            _root = root;
        }

        public object Evaluate()
        {
            return EvaluateExpression(_root);
        }

        private object EvaluateExpression(BoundExpression root)
        {
            switch (root.Kind)
            {
                case BoundNodeKind.LiteralExpression:
                    return EvaluateLiteralExpression((BoundLiteralExpression)root);
                case BoundNodeKind.BinaryExpression:
                    return EvaluateBinaryExpression((BoundBinaryExpression)root);
                case BoundNodeKind.UnaryExpression:
                    return EvaluateUnaryExpression((BoundUnaryExpression)root);
                default:
                    throw new Exception($"Unknown expression kind {root.Kind}.");
            }
        }

        private object EvaluateUnaryExpression(BoundUnaryExpression root)
        {
            var operand = (int)EvaluateExpression(root.Operand);

            switch (root.OperatorKind)
            {
                case BoundUnaryOperatorKind.Identity:
                    return operand;
                case BoundUnaryOperatorKind.Negation:
                    return -operand;
                default:
                    throw new Exception($"Unknown unary operator {root.OperatorKind}.");
            }
        }

        private object EvaluateBinaryExpression(BoundBinaryExpression root)
        {
            var left = (int)EvaluateExpression(root.Left);
            var right = (int)EvaluateExpression(root.Right);

            switch (root.OperatorKind)
            {
                case BoundBinaryOperatorKind.Addition:
                    return left + right;
                case BoundBinaryOperatorKind.Subtraction:
                    return left - right;
                case BoundBinaryOperatorKind.Multiplication:
                    return left * right;
                case BoundBinaryOperatorKind.Division:
                    return left / right;
                default:
                    throw new Exception($"Unexpected binary operator {root.OperatorKind}");
            }
        }

        private object EvaluateLiteralExpression(BoundLiteralExpression root)
        {
            return root.Value;
        }
    }
}
